module ImgPile
    ##
    # Module for accessing shared application settings
    module Settings

        ##
        # Fallback default settings to use when default file not provided
        DEFAULTS = {
            "paths": {
                "public": "",
                "uploads": "uploads"
            }
        }

        ##
        # Get the value of a setting by its path nanme
        # @param [String] name The name or path of setting
        # @return [String] Raw setting value
        # @example
        #   ImgPile::Settings::setting("paths/public")
        #
        def self.setting(name)
            level = ImgPile::SETTINGS.dup
            name.split("/").each do |part|
                level = level[part.to_sym]
            end
            return level
        end

        ##
        # Image set upload path
        # @return [String] base path for set uploads
        def self.set_path
            ImgPile::SETTINGS["paths"]["set"]
        end

        ##
        # Loads imgpile settings from yaml, optionally combining with default
        # @param [String] custom_file The custom settings file name
        # @param [String] defaults_file The default settings file name (optional)
        # @return [Hash] loaded settings
        #
        def self.load(custom_file,defaults_file=nil)
            settings = YAML.load(File.open(File.join(Rails.root,"config",custom_file)))
            defaults = (defaults_file.nil?)? ImgPile::Settings::DEFAULTS :
                YAML.load(File.open(File.join(Rails.root,"config",defaults_file)))
            return settings.fmerge(defaults)
        end

    end
end
