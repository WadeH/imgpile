## Helper methods for custom elements
module ElementHelper

    ##
    # Render header
    def header_element()
        render 'layouts/header'
    end

    ##
    # Render footer
    def footer_element()
        render 'layouts/footer'
    end

    ##
    # Render alerts
    def alerts_element()
        render 'layouts/alerts'
    end

    def panel_tag(style="default", title="", thumb_url = "", content="", footer=nil)
        parts = {has_footer: (defined? footer)? false : true}

        render "layouts/components/fake", style: style, title: title, thumb_url: thumb_url, content: content, footer: footer, parts: parts
    end

end
