class Hash
  ##
  # Less destructive implementation of Hash.merge
  # @param [Hash] Hash to add/update keys to
  # @return [Hash] Original hash with updated values from
  #   any overlapping keys in other, and any keys in
  #   other not already present in original
  # @example
  #   a = {high: { up: 1, down: -1}, still: 0}
  #   b = {high: { up: 0 }, under: -2}
  #   a.fmerge(b) => 
  #     {:high=>{:up=>1, :down=>-1}, :still=>0, :under=>-2}
  #   b.fmerge(a) =>
  #     {:high=>{:up=>0, :down=>-1}, :under=>-2, :still=>0}
  def fmerge(other)
    updated = self.dup
    new_keys = other.keys - self.keys
    new_keys.each do |k|
      updated[k] = other[k]
    end
    self.keys.each do |k|
      if other[k].is_a?(Hash) && updated[k].is_a?(Hash)
          updated[k] = updated[k].fmerge(other[k])
      else
        updated[k] = updated[k]
      end
    end
    updated
  end
end