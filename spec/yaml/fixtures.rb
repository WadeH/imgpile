##
# Provides 'fixtures'- testing yaml data stored in code
#
module YamlFixtures
  DATA = {
    "imgpile.settings1.yml": { "paths": {"public":"place"}},
    "imgpile.settings1.default.yml": { "paths": {"public":"place","uploads":"another"}}
  }
  ##
  # "Load" yaml from data hash
  # @param name The name of yaml to load
  # @return [Hash] Hash data for requested item
  #
  def self.load(name)
    YamlFixtures::DATA[name.to_sym]
  end
end