module ScriptHelper

    ##
    # Include JavaScript minified with Uglifier
    # @param [String] source JavaScript source file
    # @return [String] JavaScript tag
    # @note Automatically surrounds uglified source with <script></script> tags
    # @note Uglifier Parameters
    #   output: {
    #     beautify: true,
    #     comments: :all
    #   },
    #   mangle: false
    def js_inc(source)
        if Rails.env == "development"
            js = Uglifier.new(
                output: {
                beautify: true,
                comments: :all
                },
                mangle: false
            ).compile(render source)
        else
            js = Uglifier.new.compile(render source)
        end
        content_tag :script, js, {:type => 'text/javascript'}, false
    end

end
