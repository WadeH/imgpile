require 'rails_helper'
require 'lib_helper'

RSpec.describe ImgPile::Settings do

  context "load custom and default files" do
    it "has correct data" do
      settings = "imgpile.settings1.yml"
      defaults = "imgpile.settings1.default.yml"
      ImgPile::SETTINGS=load_settings_from_fixtures(settings,defaults)

      expected = YamlFixtures::load(settings).fmerge(YamlFixtures::load(defaults))
      expect(ImgPile::SETTINGS[:paths][:public]).to eq(expected[:paths][:public])
      expect(ImgPile::SETTINGS[:paths][:uploads]).to eq(expected[:paths][:uploads])
    end
    it "returns correct value from setting method" do
      settings = "imgpile.settings1.yml"
      defaults = "imgpile.settings1.default.yml"
      ImgPile::SETTINGS=load_settings_from_fixtures(settings,defaults)

      expected = YamlFixtures::load(settings).fmerge(YamlFixtures::load(defaults))
      expect(ImgPile::Settings::setting("paths/public")).to eq(expected[:paths][:public])
    end
  end

end