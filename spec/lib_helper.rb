require './spec/yaml/fixtures.rb'
Dir[".././app/lib/extensions/*.rb"].each { |file| load file }

def yaml_fixture_path(root,filename)
  File.join(root,"spec","yaml",filename)
end

def load_settings_from_fixtures(custom,default)
  settings = YamlFixtures::load(custom)
  defaults = (default.nil?)? ImgPile::Settings::DEFAULTS :
    YamlFixtures::load(default)
  return (defaults.nil?)? settings : settings.fmerge(defaults)
end