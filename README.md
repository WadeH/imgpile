# README

Image 'board' for displaying and organizing images.

## Uses

- Rails 5x
- Postgres
- Bootstrap
- Rspec
- ImageMagick for server-side image manipulation

## Other

- MIT License