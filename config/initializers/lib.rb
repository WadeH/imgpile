# Load ImgPile code from /lib
Dir["#{Rails.root}/app/lib/imgpile/*.rb"].each { |file| require file }
# Load extensions code from /app/lib/extensions
Dir["#{Rails.root}/app/lib/extensions/*.rb"].each { |file| require file }